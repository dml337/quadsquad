% plot_vmax_comp

set(groot,'defaultLineLineWidth',1)

% saveDataIdxs = [47 45 44 30]; % [9.3 12 16 20]
saveDataIdxs = [89 88 87 73]; % [9.3 12 16 20]
legendstr = cell(1, length(saveDataIdxs));

figure()

subplot(3,2,1)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.x)
    legendstr{i} = sprintf('V_{max}=%.2f V',saveData(saveDataIdxs(i)).mpc_obj.ManipulatedVariables(1).Max + V1_WP);
end
grid on
ylabel('x (m)')

subplot(3,2,3)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.y)
end
grid on
ylabel('y (m)')

subplot(3,2,5)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.z)
end
grid on
ylabel('z (m)')
xlabel('Time (s)')


subplot(3,2,2)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.phi)
end
grid on
ylabel('Roll (rad)')


subplot(3,2,4)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.theta)
end
grid on
ylabel('Pitch (rad)')


subplot(3,2,6)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.psi)
end
grid on
ylabel('Yaw (rad)')
xlabel('Time (s)')
legend(legendstr)

suptitle('MPC Performance, varying V_{max}')
