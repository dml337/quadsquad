function [path,V,E]=buildRRT(map,start,goal,robotRad,xRange,yRange, zRange)
% Builds RRT from start to goal
% 
%   INPUTS
%       map         NX4 matrix representing obstacle lines
%       start       start position [1X2]
%       goal        goal position [1X2]
%       robotRad    radius of the robot
%       xRange      x range of map [1X2]
%       yRange      y range of map [1X2]
% 
%   OUTPUTS
%       path        indices of vertices to go from start to goal [1XN]
%
%Miller, Jesse

%initialize
N=1000;
V=start;
E=[];
[nr,nc]=size(map);
stepSize=0.1;

for i=1:N
    %randomly sample qrand
    
    temp=10*rand;
    if temp<9
        qrand=[xRange(2)-xRange(1)-2*robotRad,yRange(2)-yRange(1)-2*robotRad].*rand(1,2)+[xRange(1)+robotRad,yRange(1)+robotRad];
    else
        qrand=goal;
    end
    
    %get qnear
    for j=1:size(V,1)
        d(j)=norm([qrand(1)-V(j,1),qrand(2)-V(j,2)]);
    end
    [~,I]=min(d);
    qnear=V(I,:);
    %get qnew
    n=[qrand(1)-qnear(1),qrand(2)-qnear(2)];
    n=n/norm(n);
    qnew=qnear+stepSize*n;
    %check if qnew to qnear is an edge in free space
    visible=isEdgeRad(map,qnear,qnew,robotRad);
    if visible
        V(end+1,1:2)=qnew;
        E(end+1,1:2)=[I,size(V,1)];
        %check if it can go to goal
        visible=isEdgeRad(map,qnew,goal,robotRad);
        if visible
            V(end+1,1:2)=goal;
            E(end+1,1:2)=[size(V,1)-1,size(V,1)];
            break
        end
    end
end
%calculate path from start to goal
path=[];
k=size(E,1);
while k~=0
    I=E(k,2);
    path(end+1,1:2)=V(I,:);
    k=find(E(:,2)==E(k,1));
end
path(end+1,1:2)=start;
path=flipud(path);

% %plot
% figure
% for k=1:size(map,1)
%     h7=plot([map(k,1),map(k,3)],[map(k,2),map(k,4)],'c','linewidth',2);
%     h1=plot([map(k,1),map(k,3)],[map(k,2),map(k,4)],'k','linewidth',3);
%     hold on
% end
% hold on
% for k=1:size(E,1)
%     v1=V(E(k,1),:); v2=V(E(k,2),:);
%     h2=plot([v1(1),v2(1)],[v1(2),v2(2)],'linewidth',2);
% end
% for k=1:size(path,1)-1
%     h4=plot([path(k,1),path(k+1,1)],[path(k,2),path(k+1,2)],'g','linewidth',3);
% end
% for k=1:size(V,1)
%     h3=plot(V(k,1),V(k,2),'r*','linewidth',2,'markersize',3);
% end
% h5=plot(start(1),start(2),'ms','markersize',5,'linewidth',4);
% h6=plot(goal(1),goal(2),'mo','markersize',5,'linewidth',4);
% h=legend([h1,h2,h3,h4,h5,h6,h7],'Workspace','Tree Edge','Tree Node','Final Path','Start','Goal','Robot Trajectory');
% set(h,'location','best','fontsize',10);
% xlabel('x (meters)','fontsize',16);
% ylabel('y (meters)','fontsize',16);
% title(sprintf('RRT Radius=%.2f m',robotRad),'fontsize',16);
%     