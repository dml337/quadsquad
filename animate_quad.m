function animate_quad(stateArray, tspan)

%% Animation
f=figure; clf; hold on;
len=0.25; % size of quadcopter
set(f,'units','normalized','outerposition',[0 0 1 1]);
axis('equal')
%set view angle
az = 5;
el = 20;
view(az, el);
hold on
grid on
% set 3D perspective on
camproj('perspective')
% set axis properties
set(gca,'Unit','normalized','Position',[0 0 1 1],'gridlinestyle','--');

% initialize axis limits
limx=10*[stateArray(1,1)-2*len,stateArray(1,1)+2*len];
limy=10*[stateArray(1,2)-1.5*len,stateArray(1,2)+1.5*len];
limz=10*[stateArray(1,3)-1.5*len,stateArray(1,3)+1.5*len];
buffer=2*len;

% initialize line objects
line1 = plot3(0,0,0,'k','linewidth',6);
line2 = plot3(0,0,0,'k','linewidth',6);

% plot ground plane
% n = 1000;
% [xplane,yplane] = meshgrid(-n:n,-n:n);
% zplane = zeros(2*n + 1);
% surf(xplane,yplane,zplane,'EdgeColor','none','FaceColor',[0.1 0.9 0.2])

dt = mean(diff(tspan));
for m = 1:length(tspan)
    % stop animation if figure is closed
    if ~ishandle(f)
        break;
    end
    
    % extract position and orientation values of COM at current time
    x=stateArray(m,1);
    y=stateArray(m,2);
    z=stateArray(m,3);
    phi=stateArray(m,4); %roll
    theta=stateArray(m,5); %pitch
    psi=stateArray(m,6); %yaw
    
    % calculate homogenious transform matrix
    H=euler2Hom([phi,theta,psi],[x,y,z]);
    
    % rotate and translate part of quadcopter
    x1=[-0.5*len,0.5*len];
    y1=[-0.5*len,0.5*len];
    z1=[0,0];
    dfi_0=[x1;y1;z1;[1,1]];
    p=H*dfi_0;
    set(line1,'xdata',p(1,:),'ydata',p(2,:),'zdata',p(3,:));
    
    % rotate and translate part of quadcopter
    x2=[0.5*len,-0.5*len];
    y2=[-0.5*len,0.5*len];
    z2=[0,0];
    dfi_0=[x2;y2;z2;[1,1]];
    p=H*dfi_0;
    set(line2,'xdata',p(1,:),'ydata',p(2,:),'zdata',p(3,:));
    
    if z < 0
        line1.Color = 'red'; line1.LineWidth = 6;
        line2.Color = 'red'; line2.LineWidth = 6;
        break;
    end
    
    %only change axis limits if quadcopter is leaving the axis limits
    if (x-buffer)-limx(1)<0
        Lx=(x-buffer)-limx(1);
    elseif (x+buffer)-limx(2)>0
        Lx=(x+buffer)-limx(2);
    else
        Lx=0;
    end
    if (y-buffer)-limy(1)<0
        Ly=(y-buffer)-limy(1);
    elseif (y+buffer)-limy(2)>0
        Ly=(y+buffer)-limy(2);
    else
        Ly=0;
    end
    if (z-buffer)-limz(1)<0
        Lz=(z-buffer)-limz(1);
    elseif (z+buffer)-limz(2)>0
        Lz=(z+buffer)-limz(2);
    else
        Lz=0;
    end
    limx=limx+Lx; limy=limy+Ly; limz=limz+Lz;
    axis([limx,limy,limz]);
    
    % title
%     titlestr = sprintf('Elapsed time: %.2f s / %.2f s',tspan(m),tspan(end));
%     title(titlestr)
    
    %pause animation
    pause(dt)
end