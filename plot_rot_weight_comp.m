% plot_rot_weight_comp

set(groot,'defaultLineLineWidth',1)

saveDataIdxs = [70 69 71 72]; % du_weight = [0.01 0.1 1 10]
legendstr = cell(1, length(saveDataIdxs));

detailstr = sprintf('Roll, Pitch, Yaw Scale Factors: [%.3f %.3f %.3f]',saveData(saveDataIdxs(1)).mpc_obj.OV(4).ScaleFactor, saveData(saveDataIdxs(1)).mpc_obj.OV(5).ScaleFactor, saveData(saveDataIdxs(1)).mpc_obj.OV(6).ScaleFactor);

figure()

%% Outputs
subplot(4,4,1:2)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.x)
    legendstr{i} = sprintf('Weight=%.2f',saveData(saveDataIdxs(i)).mpc_obj.Weights.OV(4));
end
grid on
ylabel('x (m)')

subplot(4,4,5:6)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.y)
end
grid on
ylabel('y (m)')

subplot(4,4,9:10)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.z)
end
grid on
ylabel('z (m)')
xlabel('Time (s)')


subplot(4,4,3:4)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.phi)
end
grid on
ylabel('Roll (rad)')


subplot(4,4,7:8)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.theta)
end
grid on
ylabel('Pitch (rad)')

subplot(4,4,11:12)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.psi)
end
grid on
ylabel('Yaw (rad)')
xlabel('Time (s)')
legend(legendstr)

%% Inputs
subplot(4,4,13)
hold on
for i = 1:length(saveDataIdxs)
    plot(Ts*(1:length(saveData(saveDataIdxs(i)).data.u1)), saveData(saveDataIdxs(i)).data.u1)
end
grid on
ylabel('V1 (V)')
xlabel('Time (s)')

subplot(4,4,14)
hold on
for i = 1:length(saveDataIdxs)
    plot(Ts*(1:length(saveData(saveDataIdxs(i)).data.u2)),saveData(saveDataIdxs(i)).data.u2)
end
grid on
ylabel('V2 (V)')
xlabel('Time (s)')

subplot(4,4,15)
hold on
for i = 1:length(saveDataIdxs)
    plot(Ts*(1:length(saveData(saveDataIdxs(i)).data.u3)),saveData(saveDataIdxs(i)).data.u3)
end
grid on
ylabel('V3 (V)')
xlabel('Time (s)')

subplot(4,4,16)
hold on
for i = 1:length(saveDataIdxs)
    plot(Ts*(1:length(saveData(saveDataIdxs(i)).data.u4)),saveData(saveDataIdxs(i)).data.u4)
end
grid on
ylabel('V4 (V)')
xlabel('Time (s)')

suptitle({'MPC Performance: Varying Translational Output Variable Weight'; detailstr})
