% plot_du_weight_comp

set(groot,'defaultLineLineWidth',1)

saveDataIdxs = [60 54 61 62]; % du_weight = [0.01 0.1 1 10]
legendstr = cell(1, length(saveDataIdxs));

% detailstr = sprintf('Input Scale Factors: %.3f',saveData(saveDataIdxs(1)).mpc_obj.MV.ScaleFactor);

figure()

%% Outputs
subplot(4,4,1:2)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.x)
    legendstr{i} = sprintf('Weight=%.2f',saveData(saveDataIdxs(i)).mpc_obj.Weights.MVRate(1));
end
ylabel('x (m)')

subplot(4,4,5:6)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.y)
end
ylabel('y (m)')

subplot(4,4,9:10)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.z)
end
ylabel('z (m)')
xlabel('Time (s)')


subplot(4,4,3:4)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.phi)
end
ylabel('Roll (rad)')


subplot(4,4,7:8)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.theta)
end
ylabel('Pitch (rad)')

subplot(4,4,11:12)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.psi)
end
ylabel('Yaw (rad)')
xlabel('Time (s)')
legend(legendstr)

%% Inputs
subplot(4,4,13)
hold on
for i = 1:length(saveDataIdxs)
    plot(Ts*(1:length(saveData(saveDataIdxs(i)).data.u1)), saveData(saveDataIdxs(i)).data.u1)
end
ylabel('V1 (V)')
xlabel('Time (s)')

subplot(4,4,14)
hold on
for i = 1:length(saveDataIdxs)
    plot(Ts*(1:length(saveData(saveDataIdxs(i)).data.u2)),saveData(saveDataIdxs(i)).data.u2)
end
ylabel('V2 (V)')
xlabel('Time (s)')

subplot(4,4,15)
hold on
for i = 1:length(saveDataIdxs)
    plot(Ts*(1:length(saveData(saveDataIdxs(i)).data.u3)),saveData(saveDataIdxs(i)).data.u3)
end
ylabel('V3 (V)')
xlabel('Time (s)')

subplot(4,4,16)
hold on
for i = 1:length(saveDataIdxs)
    plot(Ts*(1:length(saveData(saveDataIdxs(i)).data.u4)),saveData(saveDataIdxs(i)).data.u4)
end
ylabel('V4 (V)')
xlabel('Time (s)')

suptitle({'MPC Performance'; 'Varying Manipulated Variable Rate Weight'})
