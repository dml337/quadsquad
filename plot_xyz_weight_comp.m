% plot_xyz_weight_comp

set(groot,'defaultLineLineWidth',1)

saveDataIdxs = [65 68 67 66]; % du_weight = [0.01 0.1 1 10]
legendstr = cell(1, length(saveDataIdxs));

detailstr = sprintf('XYZ Scale Factors: %.3f',saveData(saveDataIdxs(1)).mpc_obj.OV.ScaleFactor);

figure()

%% Outputs
subplot(3,1,1)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.x)
    legendstr{i} = sprintf('Weight=%.2f',saveData(saveDataIdxs(i)).mpc_obj.Weights.OV(1));
end
grid on
ylabel('x (m)')

subplot(3,1,2)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.y)
end
grid on
ylabel('y (m)')

subplot(3,1,3)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.z)
end
grid on
ylabel('z (m)')
legend(legendstr)
xlabel('Time (s)')

suptitle({'MPC Performance: Varying Translational Output Variable Weight'; detailstr})
