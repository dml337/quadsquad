% gen_mpc_quadrotor.m
% Generates the mpc controller object for the quadrotor plant.
fprintf('Generating MPC Controller... ')

%% Setup Plant
initial_conditions % load quadrotor initial conditions
LoadQuadrotorConst_XPro1a % load quadrotor constants
old_status = mpcverbosity('off'); % suppress extra mpc toolbox messages

%% Setup MPC
Ts = 1; % sampling time (seconds)
[linplant, op, opreport] = linearize_model(Ts); % linearize the plant model

pred_horizon = 12; % prediction horizon
ctrl_horizon = 4; % control horizon

% specify mpc inputs and outputs
linplant = setmpcsignals(linplant,'MV',[1 2 3 4],'MO',[1 2 3 4 5 6]);
mpc_quad = mpc(linplant,Ts,pred_horizon,ctrl_horizon); % create mpc object

%% Modifying the MPC (see http://www.mathworks.com/help/mpc/ref/mpc.html)
% specify constraints on manipulated variables
Vmin = 0; % min input voltage
Vmax = 9.6; % max input voltage
Vrange = Vmax-Vmin; % input voltage range
mpc_quad.MV = struct('Min',{-V1_WP,-V2_WP,-V3_WP,-V4_WP},'Max',{Vmax-V1_WP,Vmax-V2_WP,Vmax-V3_WP,Vmax-V4_WP},'ScaleFactor',{Vrange,Vrange,Vrange,Vrange});
mpc_quad.OV = struct('Min',{-Inf,-Inf,0,-pi,-pi,-pi},'Max',{Inf,Inf,Inf,pi,pi,pi},'ScaleFactor',{1,1,1,1e-2,1e-2,1e-3});
% specify weights
mpc_quad.Weights = struct('MV',[.1 .1 .1 .1],'MVRate',[.1 .1 .1 .1],'OV',[0.1 0.1 0.1 0.01 0.01 0.01],'ECR',10000);

%% Noise Models
% Measurement Noise Magnitude
Measnoise = 100*[0.0001 0.0001 0.0001 0.000001*pi/180 0.000001*pi/180 0.000001*pi/180];

% Input voltage noise magnitude
Vnoise = 0.01*ones(1,4); % input voltage noise magnitude

% wind noise magnitude
Windnoise = [0.01 0.01 0.01 0.0001 0.0001 0.00001]; % [Fx Fy Fz tau_roll tau_pitch tau_yaw]

fprintf('Done\n')