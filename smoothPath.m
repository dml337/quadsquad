function pathArray=smoothPath(path,map,radius)

N=100;
k=1;

pathArray{1}=path;
while k<=N && size(pathArray{end},1)>3
    path=pathArray{end};
    n=size(path,1);
    i1=ceil(n*rand);
    i2=ceil(n*rand);
    while abs(i1-i2)<2
        i2=ceil(n*rand);
    end
    q1=path(i1,:);
    q2=path(i2,:);
    visible=isEdgeRad(map,q1,q2,radius);
    if visible
        path(min([i1,i2])+1:max([i1,i2])-1,:)=[];
        pathArray{end+1}=path;
    end
    k=k+1;
end


