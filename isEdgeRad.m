function visible=isEdgeRad(map,q1,q2,robotRad)
%determines if edge between 2 points given robot radius and map
visible=1;
for s=0:0.01:1
    q=q1*s+(1-s)*q2;
    for k=1:size(map,1)
        a = map(k,1:2); %segment points a,b
        b = map(k,3:4);
        d_ab = norm(a-b);
        d_ax = norm(a-q);
        d_bx = norm(b-q);
        if dot(a-b,q-b)*dot(b-a,q-a)>=0
            A = [a,1;b,1;q,1];
            dist = abs(det(A))/d_ab;
        else
            dist = min(d_ax, d_bx);
        end
        if robotRad>dist
            visible=0;
            break;
        end
    end
    if visible==0;
        break;
    end
end