% run_mpc_quadrotor
% Simulates quadrotor flight and saves data.

clearvars -except saveData
close all

%% Generate MPC Controller
gen_mpc_quadrotor

%% Set Waypoints and Calculate Velocity Commands
% waypts = [x_0 y_0 z_0];
% waypts = [10 0 30;
%           10 10 30;
%           10 10 40;
%           20 10 40];
waypts = [10 10 10];
% speed = 0.025; % m/s
speed = 0.1; % m/s
waypts = [x_0 y_0 z_0; waypts];
tstart = zeros(length(waypts(:,1))-1,1); tstart(1) = 0;
v_cmd = zeros(length(waypts(:,1))-1,3);
% vmax = [1 1 0.025];
for i = 2:length(waypts(:,1)); % loop through list of waypts
    dxyz = waypts(i,:) - waypts(i-1,:); % vector b/w pt i and pt i+1
    dist = norm(dxyz); % distance b/w pt i and pt i+1
    dt = dist/speed; % time it should take to go b/w pt i and pt i+1
    v_cmd(i-1,:) = dxyz/dt; % velocity vector b/w pt i and pt i+1... velocity cmds
    tstart(i) = tstart(i-1) + dt; % velocity command times
end
v_cmd = [v_cmd; [0 0 0]]; % velocity commands

% v_cmd = 0; Uncomment if using parametric reference inputs

%% Run Simulation
fprintf('Simulating Simulink model... ')
model = 'quadrotor_mpc';
tend = tstart(end);
% tend = 1000; Uncomment if using parametric reference inputs
params.StopTime = num2str(tend);
params.SimulationMode = 'normal';
params.AbsTol         = '1e-6';
params.SaveOutput     = 'on';
params.OutputSaveName = 'y';
simout = sim(model, params); % simulate!
y = simout.get('y'); u = simout.get('u'); yref = simout.get('yref');
fprintf('Done \n')

%% Plot Simulation Output
figure()
plot(y)
legend('x','y','z','phi(roll)','theta (pitch)','psi (yaw)')
figure()
plot(u)
legend('V_1','V_2','V_3','V_4');

% Extract Data
data.t = y.time;
data.x = y.data(:,1); data.xref = yref.data(:,1);
data.y = y.data(:,2); data.yref = yref.data(:,2);
data.z = y.data(:,3); data.zref = yref.data(:,3);
data.phi = y.data(:,4); data.phiref = yref.data(:,4);
data.theta = y.data(:,5); data.thetaref = yref.data(:,5);
data.psi = y.data(:,6); data.psiref = yref.data(:,6);
data.u1 = u.data(:,1);
data.u2 = u.data(:,2);
data.u3 = u.data(:,3);
data.u4 = u.data(:,4);

% Mean square errors
data.xMSE = mean((data.x - data.xref).^2);
data.yMSE = mean((data.y - data.yref).^2);
data.zMSE = mean((data.z - data.zref).^2);

% Maximum errors
errvec = [(data.x - data.xref) (data.y - data.yref) (data.z - data.zref)];
err = zeros(length(errvec),1);
for i = 1:length(errvec)
   err(i) = norm(errvec(i,:));
end
data.errmax = max(err);

% Display calculated errors
disp('Mean Square Error:')
disp(mean([data.xMSE data.yMSE data.zMSE]))
disp('Max Error:')
disp(data.errmax)

% Plot Euler Angles vs. Time
figure()
plot(data.t,data.phi,data.t,data.theta,data.t,data.psi)
title('Euler Angles vs. Time')
legend('Phi (roll)','Theta (pitch)','Psi (yaw)')
xlabel('Time (s)')
ylabel('Angle (rad)')

% Plot XYZ Trajectory
figure()
hold on
plot3(data.x,data.y,data.z,'Linewidth',2);
plot3(waypts(:,1),waypts(:,2),waypts(:,3));
legend('Actual','Desired')
scatter3(data.x(1),data.y(1),data.z(1),10,'g','filled');
scatter3(data.x(end),data.y(end),data.z(end),10,'r','filled');
for i = 1:10
    scatter3(data.x(round(i*end/10)),data.y(round(i*end/10)),data.z(round(i*end/10)),5,'k','filled');
end
view(3)
set(gca,'DataAspectRatio',[1 1 1])
title('Axis equal')
hold off
grid on
dist = norm([data.x(end),data.y(end),data.z(end)] - [data.x(1),data.y(1),data.z(1)]);
titlestr = {'Quadrotor Trajectory';
            sprintf('Total Displacement = %.2f m',dist)};
title(titlestr)
xlabel('x (m)')
ylabel('y (m)')
zlabel('z (m)')

%% Save Simulation Data to struct
if ~exist('saveData','var') && exist('saveData.mat','file')
    load('saveData.mat')
elseif ~exist('saveData','var') && ~exist('saveData.mat','file')
    namestr = sprintf('Ts=%f,p=%f,c=%f',Ts,pred_horizon,ctrl_horizon);
    saveData = struct;
    len = length(saveData);
    saveData(1).name = namestr;
    saveData(1).mpc_obj = mpc_quad;
    saveData(1).data = data;
    saveData(1).noise.V = Vnoise;
    saveData(1).noise.Wind = Windnoise;
    saveData(1).noise.Meas = Measnoise;
    save('saveData.mat','saveData');
elseif exist('saveData','var')
    namestr = sprintf('Ts=%f,p=%f,c=%f',Ts,pred_horizon,ctrl_horizon);
    len = length(saveData);
    saveData(len + 1).name = namestr;
    saveData(end).mpc_obj = mpc_quad;
    saveData(end).data = data;
    saveData(end).noise.V = Vnoise;
    saveData(end).noise.Wind = Windnoise;
    saveData(end).noise.Meas = Measnoise;
    save('saveData.mat','saveData');
end

%% Animation
% stateArray = [data.x data.y data.z data.phi data.theta data.psi];
% animate_quad(stateArray, data.t)