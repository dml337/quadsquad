% plot_mpc_comp

set(groot,'defaultLineLineWidth',2)

type = 1; % Ts_varying
% type = 2; % p_varying
% type = 3; % c_varying

switch type
    case 1
%         saveDataIdxs = [1 14 15 16]; % Ts_varying without noise
%         saveDataIdxs = [33 30 31 32]; % Ts_varying with noise [0.1 1 2.1 2.3]
        saveDataIdxs = [74 73 79 78]; % Ts_varying with noise [0.1 1 1.5 1.7]
        % goes unstable at Ts=2.3, index 16
        p = saveData(saveDataIdxs(1)).mpc_obj.PredictionHorizon;
        c = saveData(saveDataIdxs(1)).mpc_obj.ControlHorizon;
        typestr = sprintf('T_s varying, Prediction Horizon = %d, Control Horizon = %d', p, c);
    case 2
%         saveDataIdxs = [1 23]; % p_varying
%         saveDataIdxs = [38 37 30 39]; % p_varying with noise [4 8 12 16]
        saveDataIdxs = [80 81 73 82]; % p_varying with noise [4 8 12 16]
        Ts = saveData(saveDataIdxs(1)).mpc_obj.Ts;
        c = saveData(saveDataIdxs(1)).mpc_obj.ControlHorizon;
        typestr = sprintf('T_s = %f s, Prediction Horizon varying, Control Horizon = %d', Ts, c);
    case 3
%         saveDataIdxs = []; % c_varying
%         saveDataIdxs = [40 30 41 42]; % c_varying with noise [1 4 8 12]
        saveDataIdxs = [83 73 84 85];
        Ts = saveData(saveDataIdxs(1)).mpc_obj.Ts;
        p = saveData(saveDataIdxs(1)).mpc_obj.p;
        typestr = sprintf('T_s = %f s, Prediction Horizon = %d, Control Horizon varying', Ts, p);
    otherwise
        saveDataIdxs = 1:length(saveData);
        typestr = '';
end
legendstr = cell(1, length(saveDataIdxs));
% specs = {'k','--r','-.g',':b'};
specs = {'-','-','-','-','-','-'};

figure()
%% x
subplot(3,2,1)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.x, specs{mod(i,length(specs)+1)})
    
    switch type
        case 1
            legendstr{i} = sprintf('Ts=%.2f',saveData(saveDataIdxs(i)).mpc_obj.Ts);
        case 2
            legendstr{i} = sprintf('p=%.2f',saveData(saveDataIdxs(i)).mpc_obj.PredictionHorizon);
        case 3
            legendstr{i} = sprintf('c=%.2f',saveData(saveDataIdxs(i)).mpc_obj.ControlHorizon);
        otherwise
            legendstr{i} = saveData(saveDataIdxs(i)).name;
    end 
end
grid on
ylabel('x (m)')

%% y
subplot(3,2,3)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.y, specs{mod(i,length(specs)+1)})
     
end
grid on
ylabel('y (m)')

%% z
subplot(3,2,5)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.z, specs{mod(i,length(specs)+1)})
     
end
grid on
ylabel('z (m)')

%% roll
subplot(3,2,2)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.phi, specs{mod(i,length(specs)+1)})
     
end
grid on
ylabel('Roll (rad)')

%% pitch
subplot(3,2,4)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.theta, specs{mod(i,length(specs)+1)})
     
end
grid on
ylabel('Pitch (rad)')

%% yaw
subplot(3,2,6)
hold on
for i = 1:length(saveDataIdxs)
    plot(saveData(saveDataIdxs(i)).data.t,saveData(saveDataIdxs(i)).data.psi, specs{mod(i,length(specs)+1)})
     
end
grid on
legend(legendstr)
ylabel('Yaw (rad)')

% subplot(4,2,7:8)
% axis off

suptitle({'MPC Performance';typestr})
