% linearize_model.m
function [linsys, op, opreport] = linearize_model(Ts)
% Returns the linearized state space system and operating point information
% for the nonlinear quadrotor simulink model.

clearvars -except Ts
fprintf('Linearizing Simulink Model... ')

% Specify initial conditions, constants
initial_conditions
LoadQuadrotorConst_XPro1a

% Specify Inputs and Outputs
io(1) = linio('quadrotor/CL X-Pro/V1',1,'openinput');
io(2) = linio('quadrotor/CL X-Pro/V2',1,'openinput');
io(3) = linio('quadrotor/CL X-Pro/V3',1,'openinput');
io(4) = linio('quadrotor/CL X-Pro/V4',1,'openinput');
io(5) = linio('quadrotor/CL X-Pro/Body dynamics',1,'openoutput');
io(6) = linio('quadrotor/CL X-Pro/Body dynamics',2,'openoutput');
io(7) = linio('quadrotor/CL X-Pro/Body dynamics',3,'openoutput');
io(8) = linio('quadrotor/CL X-Pro/Body dynamics',4,'openoutput');
io(9) = linio('quadrotor/CL X-Pro/Body dynamics',5,'openoutput');
io(10) = linio('quadrotor/CL X-Pro/Body dynamics',6','openoutput');
model = 'quadrotor';
setlinio(model,io);

% Specify Operating Point
opspec = operspec(model);
opspec.States(1).SteadyState = 0;
opspec.States(2).SteadyState = 0;
opspec.States(3).SteadyState = 0;
opspec.States(4).SteadyState = 0;
opspec.States(5).SteadyState = 0;
opspec.States(6).SteadyState = 0;
opspec.States(7).SteadyState = 0;
opspec.States(8).SteadyState = 0;
opspec.States(9).SteadyState = 0;
opspec.States(10).SteadyState = 0;
opspec.States(11).SteadyState = 0;
opspec.States(12).SteadyState = 0;
opspec.States(13).SteadyState = 0;
opspec.States(14).SteadyState = 0;
opspec.States(15).SteadyState = 0;
opspec.States(16).SteadyState = 0;
opspec.Inputs(1).u = 0;
opspec.Inputs(2).u = 0;
opspec.Inputs(3).u = 0;
opspec.Inputs(4).u = 0;
[op, opreport] = findop(model,opspec);

% Specify Options
options = linearizeOptions('SampleTime',Ts,'UseExactDelayModel','on','RateConversionMethod','upsampling_tustin');

% Linearize System
linsys = linearize(model,op,io,options);

% Direct feedthrough not permitted. Ensure that the 'D' matrix is zero.
linsys.d = 0;
fprintf('Done\n')